import os
import logging
import time
from logging import FileHandler, Formatter

from flask import Flask, render_template, send_from_directory, request, redirect, flash, url_for
from werkzeug.utils import secure_filename
from .load_model_result import *

ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def create_app(test_config=None):
    app = Flask(__name__, instance_relative_config=True,
                instance_path='/home/dpudov/workspace/computer-architecture/bmstu-hackathon')
    app.config.from_mapping(
        SECRET_KEY='dev',
        DATABASE=os.path.join(app.instance_path, 'timetolive.sqlite'),
        UPLOAD_FOLDER=os.path.join(app.instance_path, "uploads")
    )

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)

    try:
        os.makedirs(app.instance_path)
        os.makedirs(os.path.join(app.instance_path, "uploads"))
    except OSError:
        pass

    @app.route('/favicon.ico')
    def favicon():
        return send_from_directory(os.path.join(app.root_path, 'static'),
                                   'favicon.ico', mimetype='image/vnd.microsoft.icon')

    @app.route('/', methods=['GET', 'POST'])
    def home():
        if request.method == 'POST':
            # check if the post request has the file part
            if 'file' not in request.files:
                flash('No file part')
                print('no file part')
                return redirect(request.url)
            file = request.files['file']
            # if user does not select file, browser also
            # submit an empty part without filename

            if file.filename == '':
                flash('No selected file')
                print("NO SELECTED")
                return redirect(request.url)
            if file and allowed_file(file.filename):
                filename = secure_filename(file.filename)
                print("ALLOWED PHOTO")
                print(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
                data = combine(os.path.join(app.config['UPLOAD_FOLDER'], filename)).split(' ')
                r = ''
                for i in range(len(data) - 6):
                    el = data[i].split("\n")
                    for j in range(len(el)):
                        if el[j] == "Guess":
                            r += "Guess"
                            inc = 8
                            if i == len(data) - 7:
                                inc = 7
                            for k in range(i + 1, i + inc):
                                e = data[k].split('\n')
                                r += e[0]
                            r += '\n'
                print(data)
                # print(r)
                time.sleep(3)
                return render_template('pages/placeholder.home.html',
                                       data=r)
        return render_template('pages/placeholder.home.html')

    @app.route('/about')
    def about():
        return render_template('pages/placeholder.about.html')

    @app.errorhandler(500)
    def internal_error(error):
        return render_template('errors/500.html'), 500

    @app.errorhandler(404)
    def internal_error(error):
        return render_template('errors/404.html'), 404

    if not app.debug:
        file_handler = FileHandler('error.log')
        file_handler.setFormatter(
            Formatter('%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]')
        )
        app.logger.setLevel(logging.INFO)
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)
        app.logger.info('errors')
    # from . import database
    # database.init_app(app)

    return app


if __name__ == '__main__':
    port = int(os.environ.get('PORT', 5000))
    app = create_app()
    app.run(host='0.0.0.0', port=port)
