```
conda install -c conda-forge opencv
conda install tensorflow=1.14
```

Run commands to get guess
```
python guess.py --model_type inception --model_dir /data/xdata/rude-carnie/checkpoints/age/inception/22801 --filename /home/dpressel/Downloads/portraits/prince.jpg
python guess.py --model_type inception --model_dir ~/workspace/computer-architecture/bmstu-hackathon/age-checkpoint/22801 --filename ~/Изображения/idr.jpg 
python guess.py --class_type gender --model_type inception --model_dir ~/workspace/computer-architecture/bmstu-hackathon/sex-checkpoint/21936 --filename ~/Изображения/me2.jpg 
```