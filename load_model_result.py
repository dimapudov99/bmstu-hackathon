import subprocess

MODEL_DIR_AGE = "./age-checkpoint/22801"
MODEL_DIR_GENDER = "./sex-checkpoint/21936"


def model_result_gender(img_url):
    cmd = "python rude-carnie/guess.py --class_type gender --model_type inception --model_dir {:s} --filename {:s} 2>/dev/null".format(
        MODEL_DIR_GENDER, img_url)
    p = subprocess.run(cmd, universal_newlines=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    output = p.stdout
    return output


def model_result_age(img_url):
    cmd = "python rude-carnie/guess.py --model_type inception --model_dir {:s} --filename {:s} 2>/dev/null".format(
        MODEL_DIR_AGE, img_url)
    p = subprocess.run(cmd, universal_newlines=True, shell=True, stdout=subprocess.PIPE, stderr=subprocess.DEVNULL)
    output = p.stdout
    return output


def combine(img_url):
    res = model_result_age(img_url) + "\n" + model_result_gender(img_url)
    return res
