create table if not exists images (
  id integer primary key autoincrement,
  image_url text unique not null
);

create table if not exists descriptions (
  id integer primary key autoincrement,
  image_url text not null,
  description text not null,
  foreign key (image_url) references images (image_url)
);

